package main

import (
	"bytes"
	"text/template"
)

const xmlTemplate = `
<Resource type="{{.Type}}" name="{{.Name}}" size="{{.Size}}" texture="{{.Texture}}" atlas="{{.Atlas}}">
{{range .BasisSkins}}
    <BasisSkin type="{{.Type}}" offset="{{.Offset}}" align="{{.Align}}">
        <State name="{{.StateName}}" offset="{{.StateOffset}}"/>
    </BasisSkin>
{{end}}
    <!-- <AutoSizeText offset="{{.AutoSizeText.Offset}}" align="{{.AutoSizeText.Align}}"/> -->
</Resource>
`

func generateXML(config *Config) (error, bytes.Buffer) {
	var buffer bytes.Buffer
	tmpl, err := template.New("xmlTemplate").Parse(xmlTemplate)

	if err != nil {
		return err, buffer
	}

	err = tmpl.Execute(&buffer, config)

	if err != nil {
		return err, buffer
	}

	return nil, buffer
}
