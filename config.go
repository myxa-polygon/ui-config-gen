package main

import (
	"fmt"
)

type Config struct {
	Type         string
	Name         string
	Size         string
	Texture      string
	Atlas        string
	BasisSkins   []BasisSkinData
	AutoSizeText AutoSizeTextData
}

type BasisSkinData struct {
	Type        string
	Offset      string
	Align       string
	StateName   string
	StateOffset string
}

type AutoSizeTextData struct {
	Offset string
	Align  string
}

func newBasisSkinData(image *Image, x int, y int, size SizeOfGrid) *BasisSkinData {

	var alignTable [][]string
	switch size {
	case _1X3:
		alignTable = Align1x3
	case _3X3:
		alignTable = Align3x3
	default:
		panic("Invalid argument: unknown size of grid")
	}

	offset := image.AbsCordOfCell(x, y)

	gridElement := image.grid[x-1][y-1]

	xmlOffset := fmt.Sprintf("%d %d %d %d", offset.leftTop.x, offset.leftTop.y, gridElement.Width, gridElement.Height)

	return &BasisSkinData{
		Type:        "SubSkin",
		Offset:      xmlOffset,
		Align:       alignTable[x-1][y-1],
		StateName:   "normal",
		StateOffset: xmlOffset,
	}
}

func NewBasisSkins(image *Image) []BasisSkinData {

	basisSkins := make([]BasisSkinData, image.GridX()*image.GridY())

	for i := 0; i < image.GridX(); i++ {
		for j := 0; j < image.GridY(); j++ {
			basisSkins[i*image.GridX()+j] = *newBasisSkinData(image, i+1, j+1, image.align)
		}
	}

	return basisSkins
}

func NewConfig(image *Image) *Config { // TODO: add 1x3 support

	BasisSkins := NewBasisSkins(image)

	AutoSizeText := AutoSizeTextData{
		Offset: "x y x y",
		Align:  "Stretch",
	}

	config := Config{
		Type:         "Image",
		Name:         image.name,
		Size:         fmt.Sprintf("%d %d", image.size.x, image.size.y),
		Texture:      image.texture,
		Atlas:        image.atlas,
		BasisSkins:   BasisSkins,
		AutoSizeText: AutoSizeText,
	}

	return &config
}
