package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var scanner = bufio.NewScanner(os.Stdin)
var logger = log.New(os.Stderr, "", -1)

func main() {

	var (
		name       string
		texture    string
		atlas      string = "skip"
		outputFile string = "config.xml"
	)

	var (
		size     Size
		cutCords CutCords3x3
	)

	if os.Args == nil || len(os.Args) == 1 {
		response := fmt.Sprintf("Usage: %s <texture name> <width of texture> <height of texture> <left offset> <right offset> <top offset> <bottom offset>", os.Args[0])
		fmt.Println(response)
		return
	}

	if len(os.Args) < 8 {
		logger.Fatalf("Error: not enough arguments. Expected 7, got %d\n", len(os.Args)-1)
	}

	var err error

	name = os.Args[1]
	texture = os.Args[1]

	size.x, err = strconv.Atoi(os.Args[2])
	if err != nil {
		logger.Fatalln("Error parsing width of texture:", err)
		return
	}

	size.y, err = strconv.Atoi(os.Args[3])
	if err != nil {
		logger.Fatalln("Error parsing height of texture:", err)
	}

	cutCords.left, err = strconv.Atoi(os.Args[4])
	if err != nil {
		logger.Fatalln("Error parsing left offset:", err)
	}

	cutCords.right, err = strconv.Atoi(os.Args[5])
	if err != nil {
		logger.Fatalln("Error parsing right offset:", err)
	}

	cutCords.top, err = strconv.Atoi(os.Args[6])
	if err != nil {
		logger.Fatalln("Error parsing top offset:", err)
	}

	cutCords.down, err = strconv.Atoi(os.Args[7])
	if err != nil {
		logger.Fatalln("Error parsing bottom offset:", err)
	}

	outputFile = name + ".xml"

	image := NewImage3x3(name, cutCords, size, texture, atlas)

	config := NewConfig(image)

	err, buffer := generateXML(config)

	if err != nil {
		logger.Fatalln("Error generating XML:", err)
	}

	configXml := buffer.String()

	fmt.Printf("Generated XML:\n%s\n", configXml)

	file, err := os.Create(outputFile)
	if err != nil {
		logger.Fatalln("Error creating file:", err)
	}

	absPath, err := filepath.Abs(outputFile)

	if err != nil {
		logger.Println("Error getting absolute path:", err)
		absPath = "unknown absolute path"
	}

	defer file.Close()
	_, err = file.WriteString(configXml)

	if err != nil {
		logger.Fatalln("Error writing to file:", err)
	}

	fmt.Printf("Config saved to %s: %s\n", outputFile, absPath)

	fmt.Println("Press enter to exit...")
	scanner.Scan()
}

func GetGrid() ([][]ImageCell, error) {

	grids := [][]ImageCell{}

	fmt.Println("Grids sizes:")
	for row := 1; ; row++ {
		var input string

		fmt.Printf("Row %d: ", row)

		// Scanln works bad with spaces https://stackoverflow.com/a/30156133

		scanner.Scan()
		input = scanner.Text()

		if input == "" {
			break
		}

		cells, err := parseStringRowOfGrids(input)

		if err != nil {
			return nil, err
		}

		grids = append(grids, cells)
	}

	return grids, nil
}

func parseStringRowOfGrids(input string) ([]ImageCell, error) {
	var sizes []ImageCell

	split := strings.Fields(input)

	for _, size := range split {

		size, err := parseStringSizeOfCell(size)

		if err != nil {
			return nil, err
		}
		sizes = append(sizes, size)
	}

	return sizes, nil
}

func parseStringSizeOfCell(input string) (ImageCell, error) {
	var size ImageCell

	const FORMAT = "%dx%d"

	_, err := fmt.Sscanf(input, FORMAT, &size.Width, &size.Height)

	if err != nil {
		return size, fmt.Errorf("error parsing size: %s (expected pattern: %s)", err, FORMAT)
	}

	return size, nil
}

// is there any way to avoid duplication of parseStringSize and parseStringSizeOfCell?
func parseStringSize(input string) (Size, error) {
	var size Size

	const FORMAT = "%dx%d"

	_, err := fmt.Sscanf(input, FORMAT, &size.x, &size.y)

	if err != nil {
		return size, fmt.Errorf("error parsing size: %s (expected pattern: %s)", err, FORMAT)
	}

	return size, nil
}

func parseStringCutCords(input string) (CutCords3x3, error) {
	cutCords := CutCords3x3{}

	const FORMAT = "%d %d %d %d"

	_, err := fmt.Sscanf(input, FORMAT, &cutCords.left, &cutCords.right, &cutCords.top, &cutCords.down)

	if err != nil {
		return cutCords, fmt.Errorf("error parsing cut cords: %s (expected pattern: %s)", err, FORMAT)
	}

	return cutCords, nil
}

func ScanText(prompt string) (string, error) {
	fmt.Println(prompt)
	scanner.Scan()

	scanErr := scanner.Err()

	if scanErr != nil {
		return scanner.Text(), scanErr
	}

	input := scanner.Text()
	input = strings.TrimSpace(input)
	return input, nil
}
