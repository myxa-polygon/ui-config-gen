package main

import (
	"fmt"
	"strings"
)

type ImageCell struct {
	Width  int
	Height int
}

type Cord struct {
	x int
	y int
}

type Size Cord

type Image struct {
	name    string
	grid    [][]ImageCell
	align   SizeOfGrid
	size    Size
	texture string
	atlas   string
}

type CutCords3x3 struct {
	left  int
	right int
	top   int
	down  int
}

type SizeOfGrid int

const (
	_1X3 SizeOfGrid = iota
	_3X3
)

func NewImage3x3(name string, cutCords CutCords3x3, size Size, texture string, atlas string) *Image {

	grid := make([][]ImageCell, 3)

	for i := 0; i < 2; i++ {
		grid[i] = make([]ImageCell, 3)
	}

	grid = [][]ImageCell{
		{
			ImageCell{Width: cutCords.left, Height: cutCords.top},
			ImageCell{Width: size.x - cutCords.left - cutCords.right, Height: cutCords.top},
			ImageCell{Width: cutCords.right, Height: cutCords.top}},
		{
			ImageCell{Width: cutCords.left, Height: size.y - cutCords.top - cutCords.down},
			ImageCell{Width: size.x - cutCords.left - cutCords.right, Height: size.y - cutCords.top - cutCords.down},
			ImageCell{Width: cutCords.right, Height: size.y - cutCords.top - cutCords.down}},
		{
			ImageCell{Width: cutCords.left, Height: cutCords.down},
			ImageCell{Width: size.x - cutCords.left - cutCords.right, Height: cutCords.down},
			ImageCell{Width: cutCords.right, Height: cutCords.down}},
	}

	return &Image{
		name,
		grid,
		_3X3,
		size,
		texture,
		atlas,
	}
}
func NewEmptyImage(gridSize Size) *Image {

	grid := make([][]ImageCell, gridSize.x)

	for i := range grid {
		grid[i] = make([]ImageCell, gridSize.y)
	}

	return &Image{
		name:    "",
		grid:    grid,
		size:    Size{x: 0, y: 0},
		texture: "",
		atlas:   "",
	}
}

var Align3x3 = [][]string{
	{"Left Top", "HStretch Top", "Right Top"},
	{"Left VStretch", "HStretch VStretch", "Right VStretch"},
	{"Left Bottom", "HStretch Bottom", "Right Bottom"},
}

var Align1x3 = [][]string{{"Left", "HStretch", "Right"}}

type Offset struct {
	leftTop     Cord
	rightBottom Cord
}

func (self Offset) String() string {
	return fmt.Sprintf("Верхний левый угол X=%d, Y=%d; Нижний правый угол X=%d, Y=%d", self.leftTop.x, self.leftTop.y, self.rightBottom.x, self.rightBottom.y)
}

func FormatOffsets(offesets [][]Offset) string {
	var sb strings.Builder
	for i := 0; i < len(offesets); i++ {
		for j := 0; j < len(offesets[i]); j++ {
			sb.WriteString(fmt.Sprintf("Изображение %dx%d (%d) (%s): %s\n", i+1, j+1, i*3+j+1, Align3x3[i][j], offesets[i][j]))
		}
		sb.WriteString("\n")
	}

	return sb.String()
}

func (self *Image) GridX() int {
	return len(self.grid)
}

func (self *Image) GridY() int {
	return len(self.grid[0])
}

func (self *Image) GridAbsCords() [][]Offset {

	x := self.GridX()
	y := self.GridY()

	cords := make([][]Offset, x)
	// init cords with zeros
	for i := range cords {
		cords[i] = make([]Offset, y)
	}

	for i := 0; i < x; i++ {
		for j := 0; j < y; j++ {
			cords[i][j] = self.AbsCordOfCell(i+1, j+1)
		}
	}

	return cords
}

func (self *Image) AbsCordOfCell(cordX int, cordY int) Offset {

	cords := Cord{cordX - 1, cordY - 1}

	x := 0

	for k := 0; k < cords.y; k++ {
		x += self.grid[cords.x][k].Width
	}

	y := 0

	for k := 0; k < cords.x; k++ {
		y += self.grid[k][cords.y].Height
	}

	xRight := x + self.grid[cords.x][cords.y].Width
	yBottom := y + self.grid[cords.x][cords.y].Height

	return Offset{Cord{x, y}, Cord{xRight, yBottom}}
}
